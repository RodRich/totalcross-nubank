package com.totalcross.sample.nubank;

import com.totalcross.sample.nubank.ui.Inicial;
import com.totalcross.sample.nubank.ui.SplashWindow;
import com.totalcross.sample.nubank.util.Fonts;

import totalcross.sys.Settings;
import totalcross.ui.MainWindow;
import totalcross.ui.font.Font;

public class Nubank extends MainWindow {
	
	public Nubank() {
		setUIStyle(Settings.MATERIAL_UI);
		setDefaultFont(Font.getFont(Fonts.FONT_DEFAULT_SIZE));
	}
	
	static {
		Settings.applicationId = "NUBK";
		Settings.appVersion = "1.0.0";
		Settings.iosCFBundleIdentifier = "com.totalcross.sample.nubank";
	}

	@Override
	public void initUI() {
		Inicial inicial = new Inicial();
		SplashWindow sp = new SplashWindow();
		sp.popupNonBlocking();
		
		swap(inicial);
	}		
}
