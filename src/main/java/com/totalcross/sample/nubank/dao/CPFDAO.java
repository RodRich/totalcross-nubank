package com.totalcross.sample.nubank.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.totalcross.sample.nubank.util.DatabaseManager;

import totalcross.sql.Connection;
import totalcross.sql.PreparedStatement;
import totalcross.sql.ResultSet;
import totalcross.sql.Statement;

public class CPFDAO {
	
	public void insertCPF(String cpf) throws SQLException {
		Connection connection = DatabaseManager.getConnection();
		Statement st = connection.createStatement();
		st.execute("INSERT INTO person "
				+ "		VALUES ('"+ cpf + "')");
		st.close();
	}
	
	public List<String> lerCpfs() throws SQLException {
		ArrayList<String> cpfs = new ArrayList<>();
		Connection connection = DatabaseManager.getConnection();
		Statement st = connection.createStatement();
		ResultSet rs = st.executeQuery("SELECT * FROM person");
		
		while (rs.next()) {
			cpfs.add(rs.getString("cpf"));
		}
		rs.close();
		st.close();
		return cpfs;
	}
	
	public boolean checkIfExists(String cpf) throws SQLException {
		Connection connection = DatabaseManager.getConnection();
		Statement st = connection.createStatement();
		ResultSet rs = st.executeQuery("SELECT COUNT(*) AS qtd FROM person WHERE cpf = '" + cpf + "'");
		
		boolean hadSuccess = rs.getInt("qtd") > 0;
		
		rs.close();
		st.close();
		return hadSuccess;
	}
	
	public void atualizaCpf(String oldCpf, String newCpf) throws SQLException {
		Connection connection = DatabaseManager.getConnection();
		String sql = "UPDATE person SET cpf = '" + newCpf + "'"
					+"	WHERE cpf = "+ oldCpf;
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.executeUpdate();
		ps.close();
	}
	
	public void deletaCpf(String cpf) throws SQLException {
		Connection connection = DatabaseManager.getConnection();
		String sql = "DELETE FROM person WHERE cpf = '" + cpf + "'";
		PreparedStatement ps = connection.prepareStatement(sql);
		ps.executeUpdate();
		ps.close();
	}}
