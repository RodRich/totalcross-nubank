package com.totalcross.sample.nubank.util;

import java.sql.SQLException;

import totalcross.db.sqlite.SQLiteUtil;
import totalcross.sql.Connection;
import totalcross.sql.Statement;
import totalcross.sys.Settings;

public class DatabaseManager {
	
	public static SQLiteUtil sqliteUtil;
	
	static {
		try {
			sqliteUtil = new SQLiteUtil(Settings.appPath, "nubank.db");
			
			Statement st = sqliteUtil.con()
					.createStatement();
			st.execute("CREATE TABLE IF NOT EXISTS person ("
					+ "		cpf varchar"
					+ "	)"
					);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Connection getConnection() throws SQLException {
		return sqliteUtil.con();
	}
}
