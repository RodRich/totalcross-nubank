package com.totalcross.sample.nubank.ui;

import com.totalcross.sample.nubank.util.Images;

import totalcross.ui.ImageControl;
import totalcross.ui.Label;
import totalcross.ui.Window;
import totalcross.ui.anim.ControlAnimation;
import totalcross.ui.anim.FadeAnimation;
import totalcross.ui.font.Font;
import totalcross.ui.gfx.Color;

public class SplashWindow extends Window {

	private ImageControl logo, back;

	public SplashWindow() {
	}

	@Override
	protected void onPopup() {
		Images.loadImages();

		back = new ImageControl(Images.ic_adaptive_launcher_shell_background_retang);
		back.scaleToFit = true;
		back.centerImage = true;
		back.strechImage = true;
		back.hwScale = true;
		add(back, LEFT, TOP, FILL, FILL);

		logo = new ImageControl(Images.logo_branco);
		logo.scaleToFit = true;
		logo.centerImage = true;
		logo.transparentBackground = true;
		add(logo, CENTER, CENTER, PARENTSIZE + 50, PARENTSIZE + 50);
		
		FadeAnimation.create(logo, true, null, 3000)
			.then(FadeAnimation.create(logo, false, this::onAnimationFinished, 3000))
			.start();
	}
	
	private void onAnimationFinished(ControlAnimation controlAnimation) {
		this.unpop();
	}
}
